import './App.css';
import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Pagination from '@material-ui/lab/Pagination';
import logo from "./logos/logo.svg"
const { Octokit } = require("@octokit/core");

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin:"5% 30%"
    },
  },
}));

function App() {

  const [result , setResult] = useState(null)
  const [query , setQuery] = useState(null)
  const [activePage , setActivePage] = useState(1)
  const [msgToUser , setMsgToUser] = useState(null)
  const [totalPages , setTotalPages] = useState(0)
  const classes = useStyles();

  useEffect(() =>{
      if (process.env.REACT_APP_GITHUB_API_KEY === "API_KEY")
        window.alert("Please edit the .env file for adding your github API key\nYou can easily get it from : https://github.com/settings/tokens/new?scopes=repo")
      async function fetchData() {
        const octokit = new Octokit({ auth: process.env.REACT_APP_GITHUB_API_KEY });
        const response = await octokit.request("GET /search/code", {
          q: query,
          per_page:12,
          page:activePage
        });
        if (response.status === 200){
          setMsgToUser(null)
          if (response.data.items.length === 0)
            setMsgToUser("No result found")
          let summarize = response.data.items.map(function(item){
            return{
              "name":item.repository.full_name,
              "description":item.repository.description,
              "html_url":item.repository.html_url
            }
          })
          setResult(summarize)
          setTotalPages(Math.ceil(response.data.total_count/12))
        }else {
          setMsgToUser("Something went wrong please try later")
        }
      }
      if (query!= null && query.length >= 3){
        fetchData()
      }
  },[activePage, query])

  const handleChange = (event, value) => {
  setActivePage(value)
  };

  const showResult = () =>{
    return result.map(function(item, index){
      return(
        <a key = {index} href ={item.html_url} >
          <div className="card">
            <h4 className="card-title">{item.name}</h4>
            <p className="card-description">
              {item.description}
            </p>
          </div>
        </a>
      )
    })
  }

  const pagination = () =>{
    return (
      <div className={classes.root}>
        <Pagination variant="outlined" count={totalPages} onChange={handleChange} page={activePage}/>
      </div>
    )
  }

  return (
    <div className="App">
      <div className="navbar">
          <img src={logo} className="logoD" alt="logo"/>
          <button className="btn">Plus d'infos</button>
      </div>
      <div className="back-div">
        <div className="back-div-one">
        </div>
        <div className="back-div-two">
        </div>
      </div>
      <div className="front-div">
        <div className="front-div-one">
            <div className="small-containter">
              <h1 className="title">Lorem ipsum dolor sit amet</h1>
              <p className="first-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean faucibus, nunc a molestie malesuada, tortor metus placerat nisl, non rhoncus nibh odio lacinia enim. Sed faucibus porttitor odio, eget condimentum urna ullamcorper eu. </p>
              <input className="seach" type="text" onChange={(event)=>setQuery(event.target.value)} ></input>
            </div>
        </div>
        <div className="front-div-two">
          {result != null ? showResult() : null}
          <h2 className="user-msg">{msgToUser}</h2>
        </div>
        {result != null ? pagination() : null}
      </div>
    </div>
    )
}

export default App;
